﻿# Yisoft.Nlog.Mongo
为Nlog添加MongoDB支持。

> from [loresoft/NLog.Mongo](https://github.com/loresoft/NLog.Mongo)

# License
Released under the [MIT license](http://creativecommons.org/licenses/MIT/).