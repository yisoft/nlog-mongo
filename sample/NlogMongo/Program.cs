//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;
using NLog;
using NLog.Fluent;

namespace NlogMongo
{
    internal class Program
    {
        private static readonly Logger _Logger = LogManager.GetCurrentClassLogger();

        public static void Main(string[] args)
        {
            const int k = 42;
            const int l = 100;

            _Logger.Trace("Sample trace message, k={0}, l={1}", k, l);
            _Logger.Debug("Sample debug message, k={0}, l={1}", k, l);
            _Logger.Info("Sample informational message, k={0}, l={1}", k, l);
            _Logger.Warn("Sample warning message, k={0}, l={1}", k, l);
            _Logger.Error("Sample error message, k={0}, l={1}", k, l);
            _Logger.Fatal("Sample fatal error message, k={0}, l={1}", k, l);
            _Logger.Log(LogLevel.Info, "Sample fatal error message, k={0}, l={1}", k, l);

            _Logger.Info()
                .Message("Sample informational message, k={0}, l={1}", k, l)
                .Property("Test", "Tesing properties")
                .Write();

            var path = "blah.txt";

            try
            {
                var text = File.ReadAllText(path);
            }
            catch (Exception ex)
            {
                _Logger.Error()
                    .Message("Error reading file '{0}'.", path)
                    .Exception(ex)
                    .Property("Test", "ErrorWrite")
                    .Write();
            }

            Console.WriteLine("done !");

            Console.ReadLine();
        }
    }
}
