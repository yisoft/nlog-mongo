//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Targets;
using Yisoft.Nlog.Mongo.Internal;

namespace Yisoft.Nlog.Mongo
{
    /// <summary>
    /// NLog message target for MongoDB.
    /// </summary>
    [Target("Mongo")]
    public class MongoTarget : Target
    {
        private static readonly ConcurrentDictionary<string, IMongoCollection<BsonDocument>> _CollectionCache =
            new ConcurrentDictionary<string, IMongoCollection<BsonDocument>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MongoTarget"/> class.
        /// </summary>
        public MongoTarget()
        {
            Fields = new List<MongoField>();
            Properties = new List<MongoField>();
            IncludeDefaults = true;
        }

        /// <summary>
        /// Gets the fields collection.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        [ArrayParameter(typeof(MongoField), "field")]
        public IList<MongoField> Fields { get; }

        /// <summary>
        /// Gets the properties collection.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        [ArrayParameter(typeof(MongoField), "property")]
        public IList<MongoField> Properties { get; }

        /// <summary>
        /// Gets or sets the connection string name string.
        /// </summary>
        /// <value>
        /// The connection name string.
        /// </value>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use the default document format.
        /// </summary>
        /// <value>
        ///   <c>true</c> to use the default document format; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeDefaults { get; set; }

        /// <summary>
        /// Gets or sets the name of the database.
        /// </summary>
        /// <value>
        /// The name of the database.
        /// </value>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the name of the collection.
        /// </summary>
        /// <value>
        /// The name of the collection.
        /// </value>
        public string CollectionName { get; set; }

        /// <summary>
        /// Gets or sets the size in bytes of the capped collection.
        /// </summary>
        /// <value>
        /// The size of the capped collection.
        /// </value>
        public long? CappedCollectionSize { get; set; }

        /// <summary>
        /// Gets or sets the capped collection max items.
        /// </summary>
        /// <value>
        /// The capped collection max items.
        /// </value>
        public long? CappedCollectionMaxItems { get; set; }

        /// <summary>
        /// Initializes the target. Can be used by inheriting classes
        /// to initialize logging.
        /// </summary>
        /// <exception cref="NLog.NLogConfigurationException">Can not resolve MongoDB ConnectionString. Please make sure the ConnectionString property is set.</exception>
        protected override void InitializeTarget()
        {
            base.InitializeTarget();

            if (string.IsNullOrEmpty(ConnectionString))
                throw new NLogConfigurationException(
                    "Can not resolve MongoDB ConnectionString. Please make sure the ConnectionString property is set.");
        }

        /// <summary>
        /// Writes an array of logging events to the log target. By default it iterates on all
        /// events and passes them to "Write" method. Inheriting classes can use this method to
        /// optimize batch writes.
        /// </summary>
        /// <param name="logEvents">Logging events to be written out.</param>
        [Obsolete("Instead override Write(IList<AsyncLogEventInfo> logEvents. Marked obsolete on NLog 4.5")]
        protected override void Write(AsyncLogEventInfo[] logEvents)
        {
            if (logEvents.Length == 0) return;

            try
            {
                var documents = logEvents.Select(e => _CreateDocument(e.LogEvent));

                var collection = _GetCollection();
                collection.InsertMany(documents);

                foreach (var ev in logEvents) ev.Continuation(null);
            }
            catch (Exception ex)
            {
#if NET45
				if (ex is StackOverflowException || ex is ThreadAbortException)
				throw;
#endif

                if (ex is OutOfMemoryException || ex is NLogConfigurationException) throw;

                InternalLogger.Error("Error when writing to MongoDB {0}", ex);

                foreach (var ev in logEvents) ev.Continuation(ex);
            }
        }

        /// <summary>
        /// Writes logging event to the log target.
        /// classes.
        /// </summary>
        /// <param name="logEvent">Logging event to be written out.</param>
        protected override void Write(LogEventInfo logEvent)
        {
            try
            {
                var document = _CreateDocument(logEvent);
                var collection = _GetCollection();
                collection.InsertOne(document);
            }
            catch (Exception ex)
            {
#if NET45
				if (ex is StackOverflowException || ex is ThreadAbortException)
				throw;
#endif

                if (ex is OutOfMemoryException || ex is NLogConfigurationException) throw;

                InternalLogger.Error("Error when writing to MongoDB {0}", ex);
            }
        }


        private BsonDocument _CreateDocument(LogEventInfo logEvent)
        {
            var document = new BsonDocument();

            if (IncludeDefaults || Fields.Count == 0) _AddDefaults(document, logEvent);

            // extra fields
            foreach (var field in Fields)
            {
                var value = _GetValue(field, logEvent);

                if (value == null) continue;

                document[field.Name] = value;
            }

            _AddProperties(document, logEvent);

            return document;
        }

        private void _AddDefaults(BsonDocument document, LogEventInfo logEvent)
        {
            document.Add("Date", new BsonDateTime(logEvent.TimeStamp));

            if (logEvent.Level != null) document.Add("Level", new BsonString(logEvent.Level.Name));

            if (logEvent.LoggerName != null) document.Add("Logger", new BsonString(logEvent.LoggerName));

            if (logEvent.FormattedMessage != null) document.Add("Message", new BsonString(logEvent.FormattedMessage));

            if (logEvent.Exception != null) document.Add("Exception", CreateException(logEvent.Exception));
        }

        private void _AddProperties(BsonDocument document, LogEventInfo logEvent)
        {
            var propertiesDocument = new BsonDocument();

            foreach (var field in Properties)
            {
                var key = field.Name;
                var value = _GetValue(field, logEvent);

                if (value == null) continue;

                propertiesDocument[key] = value;
            }

            var properties = logEvent.Properties ?? Enumerable.Empty<KeyValuePair<object, object>>();

            foreach (var property in properties)
            {
                if (property.Key == null || property.Value == null) continue;

                var key = Convert.ToString(property.Key, CultureInfo.InvariantCulture);
                var value = Convert.ToString(property.Value, CultureInfo.InvariantCulture);

                if (string.IsNullOrEmpty(value)) continue;

                propertiesDocument[key] = new BsonString(value);
            }

            if (propertiesDocument.ElementCount > 0) document.Add("Properties", propertiesDocument);
        }

        private BsonValue CreateException(Exception exception)
        {
            if (exception == null) return BsonNull.Value;

            var document = new BsonDocument
            {
                {"Message", new BsonString(exception.Message)},
                {"BaseMessage", new BsonString(exception.GetBaseException().Message)},
                {"Text", new BsonString(exception.ToString())},
                {"Type", new BsonString(exception.GetType().ToString())},
                {"Source", new BsonString(exception.Source)}
            };

#if NET45
			var external = exception as ExternalException;
            if (external != null)
                document.Add("ErrorCode", new BsonInt32(external.ErrorCode));

            MethodBase method = exception.TargetSite;
            if (method != null)
            {
                document.Add("MethodName", new BsonString(method.Name));

                AssemblyName assembly = method.Module.Assembly.GetName();
                document.Add("ModuleName", new BsonString(assembly.Name));
                document.Add("ModuleVersion", new BsonString(assembly.Version.ToString()));
            }
#endif

            return document;
        }


        private static BsonValue _GetValue(MongoField field, LogEventInfo logEvent)
        {
            var value = field.Layout.Render(logEvent);

            if (string.IsNullOrWhiteSpace(value)) return null;

            value = value.Trim();

            if (string.IsNullOrEmpty(field.BsonType)
                || string.Equals(field.BsonType, "String", StringComparison.OrdinalIgnoreCase)) return new BsonString(value);

            if (string.Equals(field.BsonType, "Boolean", StringComparison.OrdinalIgnoreCase)
                && value.TryBoolean(out var bsonValue)) return bsonValue;

            if (string.Equals(field.BsonType, "DateTime", StringComparison.OrdinalIgnoreCase)
                && value.TryDateTime(out bsonValue)) return bsonValue;

            if (string.Equals(field.BsonType, "Double", StringComparison.OrdinalIgnoreCase)
                && value.TryDouble(out bsonValue)) return bsonValue;

            if (string.Equals(field.BsonType, "Int32", StringComparison.OrdinalIgnoreCase)
                && value.TryInt32(out bsonValue)) return bsonValue;

            if (string.Equals(field.BsonType, "Int64", StringComparison.OrdinalIgnoreCase)
                && value.TryInt64(out bsonValue)) return bsonValue;

            return new BsonString(value);
        }

        private IMongoCollection<BsonDocument> _GetCollection()
        {
            // cache mongo collection based on target name.
            var key = string.Format("k|{0}|{1}", ConnectionString ?? string.Empty, CollectionName ?? string.Empty);

            return _CollectionCache.GetOrAdd(key, k =>
            {
                // create collection
                var mongoUrl = new MongoUrl(ConnectionString);
                var client = new MongoClient(mongoUrl);

                // Database name overrides connection string
                var databaseName = DatabaseName ?? mongoUrl.DatabaseName ?? "NLog";
                var database = client.GetDatabase(databaseName);

                var collectionName = CollectionName ?? "Log";

                if (!CappedCollectionSize.HasValue || _CollectionExists(database, collectionName)) return database.GetCollection<BsonDocument>(collectionName);

                // create capped
                var options = new CreateCollectionOptions
                {
                    Capped = true,
                    MaxSize = CappedCollectionSize,
                    MaxDocuments = CappedCollectionMaxItems
                };

                database.CreateCollection(collectionName, options);

                return database.GetCollection<BsonDocument>(collectionName);
            });
        }

        private static bool _CollectionExists(IMongoDatabase database, string collectionName)
        {
            var options = new ListCollectionsOptions
            {
                Filter = Builders<BsonDocument>.Filter.Eq("name", collectionName)
            };

            return database.ListCollections(options).ToEnumerable().Any();
        }
    }
}
