//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using MongoDB.Bson;

namespace Yisoft.Nlog.Mongo.Internal
{
    /// <summary>
    /// Convert string values to Mongo <see cref="BsonValue"/>.
    /// </summary>
    internal static class MongoConvert
    {
        /// <summary>Try to convert the string to a <see cref="BsonBoolean"/>.</summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="bsonValue">The BsonValue result.</param>
        /// <returns><c>true</c> if the value was converted; otherwise <c>false</c>.</returns>
        public static bool TryBoolean(this string value, out BsonValue bsonValue)
        {
            bsonValue = new BsonBoolean(false);

            if (value == null) return false;

            if (bool.TryParse(value, out var result))
            {
                bsonValue = new BsonBoolean(result);

                return true;
            }

            var v = value.Trim();

            if (string.Equals(v, "t", StringComparison.OrdinalIgnoreCase)
                || string.Equals(v, "true", StringComparison.OrdinalIgnoreCase)
                || string.Equals(v, "y", StringComparison.OrdinalIgnoreCase)
                || string.Equals(v, "yes", StringComparison.OrdinalIgnoreCase)
                || string.Equals(v, "1", StringComparison.OrdinalIgnoreCase)
                || string.Equals(v, "x", StringComparison.OrdinalIgnoreCase)
                || string.Equals(v, "on", StringComparison.OrdinalIgnoreCase)) bsonValue = new BsonBoolean(true);

            return true;
        }

        /// <summary>Try to convert the string to a <see cref="BsonDateTime"/>.</summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="bsonValue">The BsonValue result.</param>
        /// <returns><c>true</c> if the value was converted; otherwise <c>false</c>.</returns>
        public static bool TryDateTime(this string value, out BsonValue bsonValue)
        {
            bsonValue = null;

            if (value == null) return false;

            var r = DateTime.TryParse(value, out var result);

            if (r) bsonValue = new BsonDateTime(result);

            return r;
        }

        /// <summary>Try to convert the string to a <see cref="BsonDouble"/>.</summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="bsonValue">The BsonValue result.</param>
        /// <returns><c>true</c> if the value was converted; otherwise <c>false</c>.</returns>
        public static bool TryDouble(this string value, out BsonValue bsonValue)
        {
            bsonValue = null;

            if (value == null) return false;

            var r = double.TryParse(value, out var result);

            if (r) bsonValue = new BsonDouble(result);

            return r;
        }

        /// <summary>Try to convert the string to a <see cref="BsonInt32"/>.</summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="bsonValue">The BsonValue result.</param>
        /// <returns><c>true</c> if the value was converted; otherwise <c>false</c>.</returns>
        public static bool TryInt32(this string value, out BsonValue bsonValue)
        {
            bsonValue = null;

            if (value == null) return false;

            var r = int.TryParse(value, out var result);

            if (r) bsonValue = new BsonInt32(result);

            return r;
        }

        /// <summary>Try to convert the string to a <see cref="BsonInt64"/>.</summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="bsonValue">The BsonValue result.</param>
        /// <returns><c>true</c> if the value was converted; otherwise <c>false</c>.</returns>
        public static bool TryInt64(this string value, out BsonValue bsonValue)
        {
            bsonValue = null;

            if (value == null) return false;

            var r = long.TryParse(value, out var result);

            if (r) bsonValue = new BsonInt64(result);

            return r;
        }
    }
}
